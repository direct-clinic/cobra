package main

import (
	"direct-clinic/cobra/handler"
	"github.com/monchemin/C-19-API/connector/pgsql"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	pg, err := pgsql.Open()
	if err != nil {
		panic(err)
	}
	defer pg.Close()
	router := gin.Default()
	router.Use(cors.Default())
	router = handler.Setup(router, pg)

	if err := router.Run(":8888"); err != nil {
		panic(err)
	}
}
